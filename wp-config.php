<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/Applications/MAMP/htdocs/trueform/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'trueform');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*]2ak-0PWmc;yu8*34J!d?R6|[<7}9-#bh(  (I5|}6ZveVoX4?Mn7t!mid|lKfb');
define('SECURE_AUTH_KEY',  '/`$k8TRoehZ~]|J?KyWDe?H+)74`Z5L Hvi S4_6J,1:VN;+}l_Bq<dAAiP*r-m/');
define('LOGGED_IN_KEY',    '%J|Gd<*=j0zBW#z_xB>zkQ65qG=n|+U9E-#h!v2=2)&!4|`k_unMpuUEID3r`<+i');
define('NONCE_KEY',        '*oJ|iL;U;j%;fx&z|Liz{e=)Z~f%gagU6kN4tfc+nPf7Ce6.Z-RKAR8BUU>j,|BT');
define('AUTH_SALT',        'cZn iEDEv+hQ/.NnE>QuM`dT5KB1%Qk.(I>V8Yn?]~48-!`-%`Gq+c$%IKYVRx&`');
define('SECURE_AUTH_SALT', 'iycx,GBYxBmJr-?++ihc [N-a5/<f#xa8k/ScBt@UU@M<.P@(`<EY-3oujz$uz-m');
define('LOGGED_IN_SALT',   '%}+tzc5+J;lpY0zxXb_jG}${jhlg>YA2#]19J[gE@3|-g$-QP~i?v7Hka_!uc]q!');
define('NONCE_SALT',       '- -kz89{G:4RbQlNsb@[pb+&*-4oQXB}8/1$u}c%9`q896NfIbB;(:~-ow80nMY&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
