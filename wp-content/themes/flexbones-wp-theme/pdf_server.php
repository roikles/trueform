<?php

/*
// PHP FORCE DOWNLOAD SCRIPT
   author: pixeline
   website: http://www.pixeline.be
   last updated: 14 January 2009
*/
$filename = urldecode($_GET['file']);

// converting url to local path so Apache can find the file.
// force download:
// required for IE, otherwise Content-disposition is ignored
if (ini_get('zlib.output_compression'))
    ini_set('zlib.output_compression', 'Off');
 
$parsed_url = parse_url($filename);
$fileinfo = pathinfo($filename);
$parsed_url['extension'] = $fileinfo['extension'];
$parsed_url['filename'] = $fileinfo['basename'];
$parsed_url['localpath'] = $_SERVER['DOCUMENT_ROOT'] . $parsed_url['path'];
// just in case there is a double slash created when joining document_root and path
$parsed_url['localpath'] = preg_replace('/\/\//', '/', $parsed_url['localpath']);
 
if (!file_exists($parsed_url['localpath'])) {
    die('File not found: ' . $parsed_url['localpath']);
}
$allowed_ext = array('pdf', 'png', 'jpg', 'jpeg', 'zip', 'doc', 'xls', 'gif', 'ppt');
if (!in_array($parsed_url['extension'], $allowed_ext)) {
    die('This file type is forbidden.');
}
 
switch ($parsed_url['extension']) {
    case "pdf": $ctype = "application/pdf";
        break;
    default: $ctype = "application/force-download";
}
header("Pragma: public"); // required
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private", false); // required for certain browsers
header("Content-Type: $ctype");
header("Content-Disposition: attachment; filename=\"" . $parsed_url['filename'] . "\";");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . filesize($parsed_url['localpath']));
readfile($parsed_url['localpath']);
exit();