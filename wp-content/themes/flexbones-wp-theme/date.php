<?php get_header(); ?>

	<section class="page-content content--products" role="main"> 
		<div class="inner">

			<?php // MAIN TEXT ?>

			<div class="monthly-news">

				<h1><?php echo single_month_title(' ',false); ?> Archive</h1>

				<div class="news-content">

				
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
				
				<article class="news-post">
					<h2><?php the_title(); ?></h2>
						
					<?php the_excerpt(); ?>
						
				</article >

				<?php endwhile; ?>
				<?php endif; ?>

				</div>
			</div>

		</div>


		<?php // CAROUSEL ?>

		<div class="products-wrap">
			<h2 class="products__heading">
				NEWS ARCHIVE
			</h2>
		</div>

		<div class="products-carousel-container loading">
			<ul class="slides">
				<li class="slide">

				<?php $i = 1; ?>
			
					<?php
						$news = get_posts(  array(
							'numberposts'		=>	5,
							'category'			=>	'news',
							'orderby'			=>	'post_date',
							'order'				=>	'DESC',
							'post_type'			=>	'post',
							'post_status'		=>	'publish' )
						);
					?>
	
					<?php foreach ($news as $article): ?>
	
					<?php $articletitle = get_the_title($article); ?>
	
						<div class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 0){ echo 'product-carousel__thumb--m-last'; }?> <?php if($i % 5 == 0){ echo 'product-carousel__thumb--d-last'; }; ?>">
	
							<a href="<?php echo post_permalink($article) ?>">
								<?php $img = get_the_post_thumbnail($article->ID,'thumbnail'); ?>
								<?php if ($img) : ?>
									<?php echo $img; ?>	
								<?php else : ?>
									<img src="http://placehold.it/243x148&amp;=News" alt="<?php echo $articletitle; ?>">
								<?php endif ; ?>
								<div class="product-carousel__thumb-caption">
								<?php 
									if (get_field('news_caption',$article->ID)) :
										echo get_field('news_caption',$article->ID); 
									else :
										echo "News Article";
									endif; 
								?>
								</div>
							</a>
	
						</div>

						<?php if ($i % 15 == 0): ?>
							</li>
			
							<?php if($i !== $carousel_query->post_count): ?>
								<li class="slide">
							<?php endif; ?>
							
						<?php endif; ?>
						<?php $i++; ?>
	
					<?php endforeach ?>
				<?php if($i==$carousel_query->post_count): ?>
					</li>
				<?php endif; ?>
			</ul>
		</div>

	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>