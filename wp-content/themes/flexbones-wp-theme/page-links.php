<?php get_header(); ?>

	<section class="page-content content--products" role="main"> 
		<div class="inner">
			
			<?php // SLIDER CONTAINER ?>
			
			<div class="product-slider-container">
				
				<?php // red catalogue tab ?>
				<div class="catalogue-tab">
				<span class="helper"></span>
						<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>
				</div>
				<div class="slider big-image">
					<ul class="slides">
						<?php $img = get_the_post_thumbnail($post->ID,'main-product'); ?>
						<li><?php echo $img; ?></li>
					</ul>
				</div>
			</div>
			<?php // MAIN TEXT ?>
		<div class="product-text">
			<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
				<div class="content">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>	
				</div>
			<?php endwhile; endif; ?>
		</div>
	</div>

		<?php // CAROUSEL ?>

		<div class="products-wrap">
			<h2 class="products__heading">Links</h2>
		</div>	

		<div class="products-carousel-container loading">
			
				<ul class="slides">
				<li class="slide">

				<?php if(get_field('related_links')) : while(has_sub_field('related_links')) : ?>
	
					<div class="thumb-links__link product-carousel__thumb">
						<a href="<?php echo get_sub_field('website'); ?>">
							<?php if(get_sub_field('image')) : ?>
							<?php 
								$attachment_id = get_sub_field('image');
								$size = "thumbnail"; // (thumbnail, medium, large, full or custom size)
							 
								$img = wp_get_attachment_image_src( $attachment_id, $size );
								echo '<img src="' . $img[0] . '" alt="' . get_sub_field('caption') . '" />';
							?>
							<?php else : ?>
								<img src="http://placehold.it/243x148&amp;=No%20Image" alt="<?php echo get_sub_field('caption'); ?>">
							<?php endif; ?>
							<div class="product-carousel__thumb-caption"><?php echo get_sub_field('caption'); ?></div>
						</a>
					</div>
	
				<?php endwhile; endif; ?>
				</li>
				</ul>

		</div>

	
	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>