<?php get_header(); ?>

	<section class="main main-index" role="main"> 
		<div class="inner">
			<div class="home-hero">
				<div class="catalogue-tab">
				<span class="helper"></span>
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>
				</div>
				<div class="slider big-image">
					<ul>
						<li>
							<?php if (has_post_thumbnail()): ?>
								<?php the_post_thumbnail( 'parent-category-cover' ); ?>
							<?php else : ?>
								<img src="http://placehold.it/1016x380&amp;=%20" alt="">
							<?php endif; ?>							
						</li>
					</ul>
				</div>

				<div class="main__brief">
					<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</div>
	
			</div>
		</div>

		<?php // CAROUSEL ?>

		<div class="products-wrap">
			<h2 class="products__heading"><?php echo get_the_title(); ?></h2>
		</div>

		<div class="products-carousel-container loading">
			
			<ul class="slides">
				<li class="slide">
				<?php $i = 1; ?>

				<?php
					$id = get_the_ID();
					$depots = get_pages( 'child_of='.$id );
				?>
	
				<?php foreach ($depots as $child): ?>
	
					<?php $childtitle = get_the_title($child); ?>
	
					<div class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 0){ echo 'product-carousel__thumb--m-last'; }; if($i % 5 == 0){ echo 'product-carousel__thumb--d-last'; }; ?>">
	
						<a href="<?php echo post_permalink($child) ?>">
							<?php $img = get_the_post_thumbnail($child->ID,'thumbnail'); ?>
							<?php if ($img) : ?>
								<?php echo $img; ?>	
							<?php else : ?>
								<img src="http://placehold.it/200x148" alt="<?php echo $childtitle; ?>">
							<?php endif ; ?>
							<div class="product-carousel__thumb-caption"><?php echo $childtitle; ?></div>
						</a>
					
					</div>
					<?php $i++; ?>
				<?php endforeach ?>
			
				<?php if ($i % 15 == 0): ?>
					</li>

					<?php if($i !== $depots->post_count): ?>
						<li class="slide">
					<?php endif; ?>

				<?php endif; ?>

				<?php if($i==$depots->post_count): ?>
					</li>
				<?php endif; ?>

			</ul>

		</div>

	
	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>