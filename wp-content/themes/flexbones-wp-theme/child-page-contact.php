<?php
	/*
		Template Name: Child Contact Page
	*/
?>
<?php get_header(); ?>

	<section class="page-content content--products" role="main"> 
		<div class="inner">
			
			<?php // SLIDER CONTAINER ?>
			
			<div class="product-slider-container">
				
				<?php // red catalogue tab ?>
				<div class="catalogue-tab">
				<span class="helper"></span>
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>
				</div>
				<div class="slider big-image">
					<ul class="slides">
						<?php $img = get_the_post_thumbnail($post->ID,'main-product'); ?>
						<li><?php echo $img; ?></li>
					</ul>
				</div>
			</div>
			<?php // MAIN TEXT ?>

			<div class="product-text">
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

					<div class="content">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>

				<?php endwhile; ?>
				<?php endif; ?>

			</div>
		</div>

		<?php // CAROUSEL ?>

		<div class="products-wrap">
			<h2 class="products__heading">DEPOTS</h2>
		</div>	

		<div class="products-carousel-container loading">
			
			<ul class="slides">
			<li class="slide">
			
				<?php
					$id = "1805";
					$args = array(
						'child_of'=>$id,
						'sort_order'=>'ASC'
						);
					$depots = get_pages( $args );
				?>

				<?php $childtitle = get_the_title($depots[1]); ?>
				<?php $depot = $depots[1]; ?>

					<div class="product-carousel__thumb">
						<a href="<?php echo post_permalink($depot) ?>">
							<?php $img = get_the_post_thumbnail($depot->ID,'thumbnail'); ?>
							<?php if ($img) : ?>
								<?php echo $img; ?>	
							<?php else : ?>
								<img src="http://placehold.it/243x148" alt="<?php echo $childtitle; ?>">
							<?php endif ; ?>
						</a>
						<div class="product-carousel__thumb-caption"><?php echo $childtitle; ?></div>
					</div>

				<?php $childtitle = get_the_title($depots[0]); ?>
				<?php $depot = $depots[0]; ?>

					<div class="product-carousel__thumb product-carousel__thumb--xs-m-last">
						<a href="<?php echo post_permalink($depot) ?>">
							<?php $img = get_the_post_thumbnail($depot->ID,'thumbnail'); ?>
							<?php if ($img) : ?>
								<?php echo $img; ?>	
							<?php else : ?>
								<img src="http://placehold.it/243x148" alt="<?php echo $childtitle; ?>">
							<?php endif ; ?>
						</a>
						<div class="product-carousel__thumb-caption"><?php echo $childtitle; ?></div>
					</div>

				<?php $childtitle = get_the_title($depots[2]); ?>
				<?php $depot = $depots[2]; ?>

					<div class="product-carousel__thumb product-carousel__thumb--m-last">
						<a href="<?php echo post_permalink($depot) ?>">
							<?php $img = get_the_post_thumbnail($depot->ID,'thumbnail'); ?>
							<?php if ($img) : ?>
								<?php echo $img; ?>	
							<?php else : ?>
								<img src="http://placehold.it/243x148" alt="<?php echo $childtitle; ?>">
							<?php endif ; ?>
						</a>
						<div class="product-carousel__thumb-caption"><?php echo $childtitle; ?></div>
					</div>
			</li>
			</ul>
		</div>

	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>