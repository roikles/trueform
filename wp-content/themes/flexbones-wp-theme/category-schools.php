<?php get_header(); ?>
<?php /* Template for Schools */ ?>

		<section class="main main-index" role="main"> 

			<div class="inner">

				<div class="home-hero">
					<div class="catalogue-tab">
					<span class="helper"></span>
						<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>
					</div>
					<div class="slider big-image">
						<ul>
							<li>
							<?php //Fetch img for current category and display full width?>
							<?php 
							$category = get_category( get_query_var( 'cat' ) );
    						$cat_id = $category->cat_ID;
							$imgs = get_option( 'taxonomy_image_plugin' );
							?>
	
							<?php if (wp_get_attachment_image( $imgs[$cat_id] )): 
	
								$cover_img =  wp_get_attachment_image_src( $imgs[$cat_id], 'category-parent-cover' ); ?>
	
								<img src="<?php echo $cover_img[0]; ?>">
	
							<?php else : ?>
	
								<img  src="http://placehold.it/1016x380&amp;=Placeholder" alt="">
	
							<?php endif; ?>
							</li>
						</ul>
					</div>
					<div class="main__brief">
					<h1><?php echo single_cat_title(); ?></h1>

					<?php //Use Category Description for the block of text overlayed on image ?>
					<?php echo category_description( $cat_id ); ?>
					</div>
	
				</div>


				<?php 
					// Top level category

					//Assign Manual ID of Chosen Category until Trueform have content
					$top_cat_id = "25";
					$args = array( 'child_of' => $top_cat_id);
				?>
			</div>

			<div class="products-wrap">
				<h2 class="products__heading">
					Canopies &amp; Walkways
				</h2>
				<h2 class="slide-count">
					<span class="current-slide">
						1
					</span>
					 of 
					<span class="total-slides">
						1
					</span>
				</h2>
			</div>


				<div class="products-carousel-container loading">
					<ul class="slides">
					<li class="slide">
					<?php $i = 1; ?>
					<?php
						$wpq = custom_order($top_cat_id);
					?>

					<?php if ( $wpq->have_posts() ) : while ( $wpq->have_posts() ) : $wpq->the_post(); ?>
						<?php if($post->post_content !== ""): ?>

							<?php //Display Thumb with product rather than category link ?>
							<div class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 0){ echo 'product-carousel__thumb--m-last'; } ?> <?php if($i % 5 == 0){ echo 'product-carousel__thumb--d-last'; }; ?>">
								<a href="<?php the_permalink(); ?>?cat=<?php echo $top_cat_id; ?>">

								<?php if(get_field('product_slider')) : while(the_repeater_field('product_slider')) : ?>

									<?php
									// Pull in first image from ACF
										$rows = get_field('product_slider');
										$first_row = $rows[0];
										$first_row_image = $first_row['images'];
									?>
						
							    	<?php 
							    	// Declare Size and generate URL
							    		$size = "thumbnail";
							    		$image = wp_get_attachment_image_src( $first_row_image, $size );
							    	?>

									    
									<?php endwhile; ?>

										<?php if (is_url_exist($image[0])) : ?>
											<img src="<?php echo $image[0]; ?>" alt=""/>
										<?php else : ?>
											<img src="http://placehold.it/243x148&amp;text=No%20Image%20Available" alt="">
										<?php endif; ?>

									<?php endif; ?>

									<div class="product-carousel__thumb-caption"><?php the_title(); ?></div>
								</a>
							</div>

							
							<?php if ($i % 15 == 0): ?>
									</li>

								<?php if($i !== $post->post_count): ?>
									<li class="slide">
								<?php endif; ?>

							<?php endif; ?>
							<?php $i++; ?>
							
						<?php endif; ?>
					<?php endwhile; endif; ?>
								
					<?php if($i==$post->post_count): ?>
						</li>
					<?php endif; ?>
					</ul>

				</div>

	
	</section>
		<?php //get_sidebar(); ?>	

<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>
