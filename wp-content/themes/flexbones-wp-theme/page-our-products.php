<?php get_header(); ?>
<?php /* Template for Products CPT Archive */ ?>

		<section class="main main-index" role="main"> 

			<div class="inner">

				<div class="home-hero">
					<div class="catalogue-tab">
					<span class="helper"></span>
						<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>

					</div>
					<div class="slider big-image">
						<ul>
							<li>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/products-cover.jpg" alt="Products">
							</li>
						</ul>
					</div>

					<div class="main__brief">

					<h1><?php the_title(); ?></h1>

					<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
					</div>
	
				</div>

			</div>
			<div class="products-wrap">
				<h2 class="products__heading">Products</h2>
			</div>	
				<div class="products-carousel-container loading">
					
					<?php 
						$args = array(
							'orderby' 		=> 'id',
							'hierarchical' 	=> 1,
							'style' 		=> 'none',
							'taxonomy' 		=> 'category',
							'hide_empty' 	=> 0,
							'depth' 		=> 1,
							'title_li' 		=> '',
							'parent' 		=> 0,
							'exclude' 		=> "1,43"
						);

					$categories = get_categories($args);
					?>
					<ul class="slides">

						<li class="slide product-archive-slide">
					
							<div class="product-carousel__thumb">
								<a href="<?php echo get_category_link( "20" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[0]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb product-carousel__thumb--xs-m-last">
								<a href="<?php echo get_category_link( "21" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS2.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[1]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb product-carousel__thumb--m-last">
								<a href="<?php echo get_category_link( "22" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS3.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[2]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb product-carousel__thumb--xs-m-last">
								<a href="<?php echo get_category_link( "23" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS4.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[3]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb product-carousel__thumb--d-last">
								<a href="<?php echo get_category_link( "24" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS5.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[4]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb product-carousel__thumb--m-last product-carousel__thumb--xs-m-last">
								<a href="<?php echo get_category_link( "25" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS6.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[5]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb">
								<a href="<?php echo get_category_link( "26" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/DESIGN-BUILD-METALWORK-THUMB.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[6]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb product-carousel__thumb--xs-m-last">
								<a href="<?php echo get_category_link( "27" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/ELECTRONIC-DISPLAYS-THUMB.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[7]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb product-carousel__thumb--m-last">
								<a href="<?php echo get_category_link( "28" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS9.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[8]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb product-carousel__thumb--d-last product-carousel__thumb--xs-m-last">
								<a href="<?php echo get_category_link( "29" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS10.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[9]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb">
								<a href="<?php echo get_category_link( "30" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS11.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[10]->cat_name; ?></div>
								</a>
							</div>
	
							<div class="product-carousel__thumb product-carousel__thumb--m-last product-carousel__thumb--xs-m-last">
								<a href="<?php echo get_category_link( "31" ) ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS12.jpg" alt="Homepage Thumbnail" />
									<div class="product-carousel__thumb-caption"><?php echo $categories[11]->cat_name; ?></div>
								</a>
							</div>

						</li>
					</ul>
					
				</div>
			

	</section>
		<?php //get_sidebar(); ?>	

<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>
