# Require any additional compass plugins here.

output_style = :compressed
preferred_syntax = :scss
Sass::Script::Number.precision = 10
http_path = "/"
css_dir = "/"
sass_dir = "/style/sass"
relative_assets = false
line_comments = false
sass_options = {:debug_info=>false} 
images_dir = "/images"
javascripts_dir = "/js"