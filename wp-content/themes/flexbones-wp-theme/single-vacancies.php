<?php get_header(); ?>
<?php // Loop through all posts under current tag ?>
<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
<section class="page-content content--products" role="main"> 
		<div class="inner">
			
			<?php // SLIDER CONTAINER ?>
			
			<div class="product-slider-container">
				
				<?php // red catalogue tab ?>
				<div class="catalogue-tab">
				<span class="helper"></span>
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>
				</div>
				<?php // SLIDER INNER ?>
				<div class="slider big-image">
					<ul class="slides">
						<li>
							<?php $terms = wp_get_post_terms( $post->ID, "regions" ); ?>
							<?php $current_tax = $terms[0]->slug; ?>
							<?php // Conditional Statement to determine which image to display?>
							<?php if ($current_tax == 'uk-europe') : ?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/UK-AND-EUROPE.jpg"/>
							<?php elseif ($current_tax == 'us-canada') : ?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/US-CANADA.jpg"/>
							<?php elseif ($current_tax == 'middle-east-uae') : ?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/MIDDLE-EAST-UAE.jpg"/>
							<?php endif; ?>
						</li>
					</ul>
				</div>	

			</div>
			<?php // MAIN TEXT ?>

			<div class="product-text">

			

				<?php // Pull in all ACF Fields per vacancy ?>
				<?php if (get_field('job_vacancy')) : ?>

				

					<?php while(has_sub_field('job_vacancy')) :?>

					<?php 
						$role = get_sub_field('job_role');
						$location = get_sub_field('location');
						$salary  = get_sub_field('salary');
						$date  = get_sub_field('date_posted');
						$job_type  = get_sub_field('job_type');
						$brief  = get_sub_field('short_description');
						$description  = get_sub_field('long_description');
					?>

					
					<div class="content">
						<h1><?php echo $role; ?></h1>
						<strong>Location: <?php echo $location; ?></strong><br>
						<strong>Salary: <?php echo $salary; ?></strong><br>
						<strong>Date Posted: <?php echo $date; ?></strong><br>
						<strong>Job Type: <?php echo $job_type; ?></strong><br>
						<p><?php echo $description; ?></p>
						<h3>To apply for this job please email your CV and cover letter to 
							<a href='mailt&#111;&#58;h%6&#49;y%6Cey&#46;%72&#117;sse&#108;%6&#67;&#64;&#116;%&#55;2uef%6Fr%6D&#46;%63o&#46;u&#37;6B'>
								&#104;&#97;yle&#121;&#46;russ&#101;&#108;l&#64;&#116;&#114;uefor&#109;&#46;&#99;o&#46;uk
							</a>
						</h3>
					<?php endwhile; ?>

				<?php endif; ?>

			<?php endwhile; else : ?>

					<?php // No Posts under this region ?>
					<h2>Sorry we have no available vacancies at the moment.</h2>

			<?php endif; ?>

			<?php // Reset $POST ?>
			<?php wp_reset_postdata(); ?>
				</div>
			</div>

		</div>
		<div class="products-wrap">
			<h2 class="products__heading">
				Regions
			</h2>
		</div>


		<div class="products-carousel-container loading">

			<?php // Output Image thumbnail links ?>
			<figure class="product-carousel__thumb">
			<a href="<?php echo site_url('regions/uk-europe'); ?>">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/UK-AND-EUROPE-thumb.jpg" alt="UK &amp; Europe"/>
				<figcaption class="product-carousel__thumb-caption">
					UK &amp; Europe
				</figcaption>
			</a>
			</figure>

			<figure class="product-carousel__thumb product-carousel__thumb--xs-m-last">
			<a href="<?php echo site_url('regions/middle-east-uae'); ?>">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/MIDDLE-EAST-UAE-thumb.jpg" alt="Middle East &amp; UAE"/>
				<figcaption class="product-carousel__thumb-caption">
					Middle East &amp; UAE
				</figcaption>
			</a>
			</figure>

			<figure class="product-carousel__thumb">
			<a href="<?php echo site_url('regions/us-canada'); ?>">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/US-CANADA-thumb.jpg" alt="US &amp; Canada"/>
				<figcaption class="product-carousel__thumb-caption">
					US &amp; Canada
				</figcaption>
			</a>
			</figure>
				
		</div>

	</section>

<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>
