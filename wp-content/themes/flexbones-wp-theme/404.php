<?php get_header(); ?>

	<section class="error page-content" role="main"> 
		<div class="inner">
			<h1>Sorry! The page you were looking for was not found</h1>
			
		</div>
	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>