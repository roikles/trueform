<?php /* The template for the Search form. */ ?>
<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
	<div class="search-form">
		<input type="text" value="Enter Search" name="s" id="s" style="color:#fff" class="search-input" placeholder="Enter Search" onfocus="if (this.value == 'Enter Search') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Enter Search';}" />
		<input type="submit" value="" name="search-button" class="search-button">
	</div>
</form>