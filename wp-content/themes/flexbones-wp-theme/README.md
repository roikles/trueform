Flexbones Wordpress Theme
=========

1. Features
--------

* __Lightweight__
* __Responsive__ 
* __Minimalistic Template Approach__ 
* __Modular SASS__ 
* __Facebook style slideout menu__
* __JenkinsBreakpoint.js__

2. Overview
--------

Not another barebones theme! I built this to speed up the process of developing responsive wordpress themes. It uses SASS/ Compass and the Susy Grid system to allow speedy development.

The original Barebones theme contained everything and the kitchen sink, This time around I have removed as much uneccesary bloat as possible.

The included sass styles are focused on functional CSS rather than presentational CSS. Flexbones provides not only a responsive grid system but responsive images and typography.* 

*Responsive typography sizes are not based on the sort of in depth research that IA (http://ia.net/) have conducted. That is up to you. To begin with the base typographic units are set to 10px desktop 9px tablet and 8px mobile. This decision was made to make the source understandable not to provide an optimal reading experience. Perhaps this will come in the future.


3. Dependencies
--------

CSS Preprocessing tools

* [SASS](http://sass-lang.com)
* [Compass](http://compass-style.org)
* [Susy](http://susy.oddbird.net/)


