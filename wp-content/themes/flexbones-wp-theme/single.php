<?php get_header(); ?>

	<section class="page-content content--products" role="main"> 
		<div class="inner">
			
			<?php // SLIDER CONTAINER ?>
			
			<div class="product-slider-container">
				<div class="catalogue-tab">
				<span class="helper"></span>
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>

				</div>
				<div class="slider big-image">
					<ul class="slides">
						<li>
						<?php if (has_post_thumbnail()): ?>
							<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'main-product' ); ?>
							<img src="<?php echo $image[0]; ?>" alt="Article Thumbnail"> 
						<?php else : ?>
							<img src="http://placehold.it/500x380&amp;=%20" alt="">
						<?php endif; ?>			
						</li>
					</ul>
				</div>				
	
			</div>
			<?php // MAIN TEXT ?>

			<div class="product-text">
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

				<div class="content">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>

				<?php endwhile; ?>
				<?php endif; ?>

			</div>
		</div>

		<?php // CAROUSEL ?>

		<div class="products-wrap">
			<h2 class="products__heading">
				NEWS ARCHIVE
			</h2>
		</div>

		<div class="products-carousel-container loading">
			<ul class="slides">
				<li class="slide">

				<?php $i = 1; ?>
			
					<?php
						$news = get_posts(  array(
							'numberposts'		=>	5,
							'category'			=>	'news',
							'orderby'			=>	'post_date',
							'order'				=>	'DESC',
							'post_type'			=>	'post',
							'post_status'		=>	'publish' )
						);
					?>
	
					<?php foreach ($news as $article): ?>
	
					<?php $articletitle = get_the_title($article); ?>
	
						<div class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 0){ echo 'product-carousel__thumb--m-last'; }?> <?php if($i % 5 == 0){ echo 'product-carousel__thumb--d-last'; }; ?>">
	
							<a href="<?php echo post_permalink($article) ?>">
								<?php $img = get_the_post_thumbnail($article->ID,'thumbnail'); ?>
								<?php if ($img) : ?>
									<?php echo $img; ?>	
								<?php else : ?>
									<img src="http://placehold.it/243x148&amp;=News" alt="<?php echo $articletitle; ?>">
								<?php endif ; ?>
								<div class="product-carousel__thumb-caption">
								<?php 
									if (get_field('news_caption',$article->ID)) :
										echo get_field('news_caption',$article->ID); 
									else :
										echo "News Article";
									endif; 
								?>
								</div>
							</a>
	
						</div>

						<?php if ($i % 15 == 0): ?>
							</li>
			
							<?php if($i !== $carousel_query->post_count): ?>
								<li class="slide">
							<?php endif; ?>
							
						<?php endif; ?>
						<?php $i++; ?>
	
					<?php endforeach ?>
				<?php if($i==$carousel_query->post_count): ?>
					</li>
				<?php endif; ?>
			</ul>
		</div>

	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>