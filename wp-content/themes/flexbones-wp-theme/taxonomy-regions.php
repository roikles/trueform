<?php get_header(); ?>

<?php $current_tax = get_query_var('regions'); ?>

<section class="page-content content--products" role="main"> 
		<div class="inner">
			
			<?php // SLIDER CONTAINER ?>
			
			<div class="product-slider-container">
				
				<?php // red catalogue tab ?>
				<div class="catalogue-tab hidden">
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>
				</div>
				<div class="slider big-image">
					<ul class="slides">					
						<li>
							<?php // Conditional Statement to determine which image to display?>
							<?php if ($current_tax == 'uk-europe') : ?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/UK-AND-EUROPE.jpg"/>
							<?php elseif ($current_tax == 'us-canada') : ?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/US-CANADA.jpg"/>
							<?php elseif ($current_tax == 'middle-east-uae') : ?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/MIDDLE-EAST-UAE.jpg"/>
							<?php endif; ?>
						</li>
					</ul>
				</div>
			</div>
			<?php // MAIN TEXT ?>

			<div class="product-text">
				
				<h1 class="region-vacancy-heading">Vacancies - 
					<?php if ($current_tax == 'uk-europe') :
						echo "UK &amp; Europe";
					elseif ($current_tax == 'us-canada') :
						echo "US &amp; Canada";
					elseif ($current_tax == 'middle-east-uae') :
						echo "Middle East &amp; UAE";
					endif; ?>
				</h1>
	
				<div class="content">
	
				<?php // Set arguments for Taxonomy query using current tag ?>
				<?php $region_args = array(
						'post_type' => 'vacancies',
						'regions' => $current_tax,
						'order_by' => 'name',
						'order' => 'ASC' );
	
				// Set up WP_Query
				$query = new WP_Query($region_args); ?>
	
				<?php // Loop through all posts under current tag ?>
				<?php if ( $query->have_posts() ): while ( $query->have_posts() ) : $query->the_post(); ?>
	
					<?php // Pull in all ACF Fields per vacancy ?>
					<?php if (get_field('job_vacancy')) : ?>
	
						<?php while(has_sub_field('job_vacancy')) :?>
	
							<?php 
								$role = get_sub_field('job_role');
								$location = get_sub_field('location');
								$salary  = get_sub_field('salary');
								$date  = get_sub_field('date_posted');
								$job_type  = get_sub_field('job_type');
								$brief  = get_sub_field('short_description');
								$description  = get_sub_field('long_description');
							?>
	
							<h2><?php echo $role; ?></h2>
								<strong>Location: <?php echo $location; ?></strong><br>
								<strong>Salary: <?php echo $salary; ?></strong><br>
								<strong>Date Posted: <?php echo $date; ?></strong><br>
								<strong>Job Type: <?php echo $job_type; ?></strong><br>
								<p><?php echo $brief; ?></p>
								<a class="vacancy-link" href="<?php echo get_permalink($query->ID); ?>">View Job...</a>
	
						<?php endwhile; ?>
	
					<?php endif; ?>
	
				<?php endwhile; else : ?>
	
					<?php // No Posts under this region ?>
					<h2>Sorry we have no available vacancies at the moment.</h2>
	
				<?php endif; ?>
	
				<?php // Reset $POST ?>
				<?php wp_reset_postdata(); ?>
				</div>

			</div>
		</div>

		<div class="products-wrap">
			<h2 class="products__heading">Regions</h2>
		</div>

		<div class="products-carousel-container loading">
			<ul class="slides">
				<li class="slide">
					<?php // Output Image thumbnail links ?>
				<div class="product-carousel__thumb">
					<a href="<?php echo site_url('regions/uk-europe'); ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/UK-AND-EUROPE-thumb.jpg" alt="UK &amp; Europe"/>
						<div class="product-carousel__thumb-caption">
							UK &amp; Europe
						</div>
					</a>
				</div>
	
				<div class="product-carousel__thumb product-carousel__thumb--xs-m-last">
					<a href="<?php echo site_url('regions/middle-east-uae'); ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/MIDDLE-EAST-UAE-thumb.jpg" alt="Middle East &amp; UAE"/>
						<div class="product-carousel__thumb-caption">
							Middle East &amp; UAE
						</div>
					</a>
				</div>
	
				<div class="product-carousel__thumb">
					<a href="<?php echo site_url('regions/us-canada'); ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/US-CANADA-thumb.jpg" alt="US &amp; Canada"/>
						<div class="product-carousel__thumb-caption">
							US &amp; Canada
						</div>
					</a>
				</div>
				</li>
			</ul>
				
		</div>

	</section>

<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>
