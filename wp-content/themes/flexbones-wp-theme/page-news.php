<?php get_header(); ?>

	<section class="page-content content--products" role="main"> 
		<div class="inner">
			
			<?php // SLIDER CONTAINER ?>
			
			<div class="product-slider-container">
				
				<?php // red catalogue tab ?>
				<div class="catalogue-tab">
				<span class="helper"></span>
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>
				</div>
				<div class="slider big-image">
					<ul>
						<?php $img = get_the_post_thumbnail($post->ID,'main-product'); ?>
						<li><?php echo $img; ?></li>
					</ul>
				</div>
			</div>
			<?php // MAIN TEXT ?>
					
		<?php 
			$newsarchive = get_posts(  array(
				'cat' 				=> 'news',
				'numberposts'		=>	5,
				'orderby'			=>	'post_date',
				'order'				=>	'DESC',
				'post_type'			=>	'post',
				'post_status'		=>	'publish' )
			);
		?>
			
			<div class="product-text">

				<div class="content">
					<h1><?php the_title(); ?></h1>

					<?php foreach ( $newsarchive as $post ) : setup_postdata( $post ); ?>

						<h2><?php the_title(); ?> - <?php the_time(get_option('date_format')); ?></h2>

						<?php the_excerpt(); ?>

					<?php endforeach; ?> 

					<?php $args = array(
						'type'            => 'monthly',
						'format'          => 'html', 
						'before'          => '',
						'after'           => '',
						'show_post_count' => false,
						'echo'            => 1,
						'order'           => 'DESC'
					); ?>
			
					<div class="archives">
						<h2>Monthly Archive</h2>
						<?php wp_get_archives( $args ); ?>
					</div>
					<?php wp_reset_query(); ?>

				</div>
			</div>

		</div>

		<?php // CAROUSEL ?>

		<div class="products-wrap">
			<h2 class="products__heading">
				NEWS ARCHIVE
			</h2>
		</div>

		<div class="products-carousel-container loading">
			<ul class="slides">
				<li class="slide">

				<?php $i = 1; ?>
			
					<?php
						$news = get_posts(  array(
							'numberposts'		=>	5,
							'category'			=>	'news',
							'orderby'			=>	'post_date',
							'order'				=>	'DESC',
							'post_type'			=>	'post',
							'post_status'		=>	'publish' )
						);
					?>
	
					<?php foreach ($news as $article): ?>
	
					<?php $articletitle = get_the_title($article); ?>
	
						<div class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 0){ echo 'product-carousel__thumb--m-last'; }?> <?php if($i % 5 == 0){ echo 'product-carousel__thumb--d-last'; }; ?>">
	
							<a href="<?php echo post_permalink($article) ?>">
								<?php $img = get_the_post_thumbnail($article->ID,'thumbnail'); ?>
								<?php if ($img) : ?>
									<?php echo $img; ?>	
								<?php else : ?>
									<img src="http://placehold.it/243x148&amp;=News" alt="<?php echo $articletitle; ?>">
								<?php endif ; ?>
								<div class="product-carousel__thumb-caption">
								<?php 
									if (get_field('news_caption',$article->ID)) :
										echo get_field('news_caption',$article->ID); 
									else :
										echo "News Article";
									endif; 
								?>
								</div>
							</a>
	
						</div>

						<?php if ($i % 15 == 0): ?>
							</li>
			
							<?php if($i !== $carousel_query->post_count): ?>
								<li class="slide">
							<?php endif; ?>
							
						<?php endif; ?>
						<?php $i++; ?>
	
					<?php endforeach ?>
				<?php if($i==$carousel_query->post_count): ?>
					</li>
				<?php endif; ?>
			</ul>
		</div>

	
	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>