<?php get_header(); ?>

<?php 
	if (isset($wp_query->query_vars['cat'])) $master_cat = esc_attr($wp_query->query_vars['cat']);
?>

	<section class="page-content content--products" role="main">
		<div class="inner">
			
			<?php // SLIDER CONTAINER ?>
			
			<div class="product-slider-container">
				
				<?php // red catalogue tab ?>
				<div class="catalogue-tab">
				<span class="helper"></span>
						<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>
				</div>
				<?php // SLIDER INNER ?>
				<div class="slider big-image">
					<ul class="slides">
						<?php if( get_field('product_slider') ): while ( has_sub_field('product_slider') ) : ?>
							<li>
								<?php $image = wp_get_attachment_image_src( get_sub_field('images'), "main-product" ); ?>
								<img src="<?php echo $image[0]; ?>" />
							</li>
						<?php endwhile; endif; ?>
					</ul>
				</div>
			</div>
			<?php // MAIN TEXT ?>

			<div class="product-text">

				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
				
					<div class="product-text__pdf">
						<a href="<?php echo get_stylesheet_directory_uri(); ?>/pdf_server.php?file=<?php the_field('individual_product_pdf'); ?>" target="_blank">
							<p>Download PDF file</p>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pdf-icon.png" class="product-text__pdf-icon">
						</a>
					</div>
					<div class="content">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>

				<?php endwhile; ?>
				<?php endif; ?>

			</div>
		</div>

		<?php // CAROUSEL ?>

		<div class="products-wrap">
			<h2 class="products__heading">
				Products / <?php echo get_the_category_by_ID( $master_cat )?>
			</h2>
			<h2 class="slide-count">
				<span class="current-slide">1</span> of <span class="total-slides">1</span>
			</h2>
		</div>

		<div class="products-carousel-container loading">

			<ul class="slides">
				<li class="slide">
					
					<?php require 'includes/product-carousel-slide.php'; ?>
				
 				<?php if($i==$wpq->post_count): ?>
					</li>
				<?php endif; ?>
			</ul>
		</div>
	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>