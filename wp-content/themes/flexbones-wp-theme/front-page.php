<?php get_header(); ?>
	
	<section class="page-content content--index" role="main"> 
		
		<div class="inner">
			<div class="home-hero">
				<div class="catalogue-tab catalogue-tab--homepage">
				<span class="helper"></span>
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" >
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue">
						</a>
				</div>
				<?php if( get_field('homepage_slider') ): ?>
				<div class="slider big-image">
					<ul class="slides">

						<?php while ( has_sub_field('homepage_slider') ) : ?>
							<li>
								
								<?php $image = wp_get_attachment_image_src( get_sub_field('image'), "homepage-slider" ); ?>
								<img src="<?php echo $image[0]; ?>"  alt="Trueform Homepage Slider Image" />
							</li>
						<?php endwhile; ?>
					</ul>
				</div>
				<?php endif; ?>
				<div class="main__brief">
					<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</div>
			</div>
			
		</div>


			<div class="main__content">
				<div class="main__content--news">
					<h2>News</h2>

					<?php
					    $news_query = new WP_Query(array(
					        'cat' => 'uncategorized',
					        'posts_per_page' => 4,
					        'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1
					    ));

					    if ( $news_query->have_posts() ): while ( $news_query->have_posts() ) : $news_query->the_post(); ?>

					   <article class="news-stub">
					    	<p><?php the_time(get_option('date_format')); ?></p>
					    	<p class="news__title"><?php the_title(); ?></p>
					    	<?php the_excerpt(); ?>
			    		</article>
					        	
			    	<?php endwhile; endif;	?>
				</div>
				<div class="main__content--categories">
					<?php 
						$args = array(
							'orderby' => 'id',
							'hierarchical' => 1,
							'style' => 'none',
							'taxonomy' => 'category',
							'hide_empty' => 0,
							'depth' => 1,
							'title_li' => '',
							'parent' => 0,
							'exclude' => "1,43"
						);
					$imgs = get_option( 'taxonomy_image_plugin' );
					$categories = get_categories($args);
					?>
					
						<figure class="cat-thumb">
							<a href="<?php echo get_category_link( "20" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS.jpg" alt="<?php echo $categories[0]->cat_name; ?>" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[0]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb cat-thumb-xs-m-last">
							<a href="<?php echo get_category_link( "21" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS2.jpg" alt="<?php echo $categories[1]->cat_name; ?>" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[1]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb cat-thumb--m-last">
							<a href="<?php echo get_category_link( "22" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS3.jpg" alt="<?php echo $categories[2]->cat_name; ?>" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[2]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb cat-thumb--d-last cat-thumb-xs-m-last">
							<a href="<?php echo get_category_link( "23" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS4.jpg" alt="<?php echo $categories[3]->cat_name; ?>" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[3]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb">
							<a href="<?php echo get_category_link( "24" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS5.jpg" alt="<?php echo $categories[4]->cat_name; ?>" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[4]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb cat-thumb--m-last cat-thumb-xs-m-last">
							<a href="<?php echo get_category_link( "25" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS6.jpg" alt="Homepage Thumbnail" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[5]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb">
							<a href="<?php echo get_category_link( "26" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/DESIGN-BUILD-METALWORK-THUMB.jpg" alt="Homepage Thumbnail" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[6]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb cat-thumb--d-last cat-thumb-xs-m-last">
							<a href="<?php echo get_category_link( "27" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/ELECTRONIC-DISPLAYS-THUMB.jpg" alt="Homepage Thumbnail" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[7]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb cat-thumb--m-last">
							<a href="<?php echo get_category_link( "28" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS9.jpg" alt="Homepage Thumbnail" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[8]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb cat-thumb-xs-m-last">
							<a href="<?php echo get_category_link( "29" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS10.jpg" alt="Homepage Thumbnail" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[9]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb">
							<a href="<?php echo get_category_link( "30" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS11.jpg" alt="Homepage Thumbnail" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[10]->cat_name; ?></figcaption>
							</a>
						</figure>

						<figure class="cat-thumb cat-thumb--m-last cat-thumb--d-last cat-thumb-xs-m-last">
							<a href="<?php echo get_category_link( "31" ) ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage_thumbs/HOMEPAGE%20THUMBNAILS12.jpg" alt="Homepage Thumbnail" />
								<figcaption class="cat-thumb__caption"><?php echo $categories[11]->cat_name; ?></figcaption>
							</a>
						</figure>
						
				</div>
			

			</div>
			
		

	</section>

<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>