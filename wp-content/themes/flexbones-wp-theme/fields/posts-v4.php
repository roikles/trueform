<?php

class acf_field_post_list extends acf_field
{
	// vars
	var $settings, // will hold info such as dir / path
		$defaults; // will hold default field options


	/*
	*  __construct
	*
	*  Set name / label needed for actions / filters
	*
	*  @since	3.6
	*  @date	23/01/13
	*/

	function __construct()
	{
		// vars
		$this->name = 'post_list';
		$this->label = __('Post List');
		$this->category = __("Relational",'acf'); // Basic, Content, Choice, etc
		$this->defaults = array(
			// add default here to merge into your field.
			// This makes life easy when creating the field options as you don't need to use any if( isset('') ) logic. eg:
			//'preview_size' => 'thumbnail'
		);


		// do not delete!
    parent::__construct();


    // settings
		$this->settings = array(
			'path' => apply_filters('acf/helpers/get_path', __FILE__),
			'dir' => apply_filters('acf/helpers/get_dir', __FILE__),
			'version' => '1.0.0'
		);

	}


	/*
	*  field_group_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is edited.
	*  Use this action to add css + javascript to assist your create_field_options() action.
	*
	*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function field_group_admin_enqueue_scripts(){
	  	
	}

	/*
	*  create_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field - an array holding all the field's data
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function create_field( $field )	{

		$option = $_POST['option_name'];

		if (empty($option)) {
			echo '
				<script>
					jQuery(document).ready(function ($) {
						$("#acf-post_menu_order").hide();
					});
				</script>
				';
			exit();
		}else{
			$current_cat = str_replace('category_', '', $option);
		}
	

		// Make sure that we are definately in the Admin area
		if (is_admin()) { ?>

			<script>
				jQuery(document).ready(function ($) {
					$( ".sortable" ).sortable({
						cursor: "move",
						opacity: 0.7,
						placehold: "sortable-post",
						containment: ".sortable-container",
						delay: 150,
						change: function(event, ui){
							$( ".check-success p" ).text("");
						}
					});
					$( ".sortable" ).disableSelection();

					$(".acf-save-order-button").click(function(){
						//console.log("CLICK: Saving menu_order");
						$.ajax({
				            type: "POST",
				            url: "<?php echo get_stylesheet_directory_uri(); ?>/fields/custom-order.php",
				            data: $(".sortable").sortable("serialize"),
				            context: document.body,
				            success: function(){
				           		//console.log("AJAX: menu_order stored!");
				           		$( ".check-success p" ).text("Changes have been saved.");
				        	}
					    });
					});
				});
			</script>

		<?php } ?>






		<?php

		check_for_additions($current_cat);
		check_for_removals($current_cat);

		$the_query = custom_query_order($current_cat);

		// The Loop
		$marker = '&#8597;';
		if ( $the_query->have_posts() ) :
			echo '<div class="sortable-container">';
		        echo '<ul class="sortable">';
			while ( $the_query->have_posts() ) :
				$the_query->the_post();
				
				echo '<li id="' . $current_cat . '_' . get_the_ID() . '" class="ui-state-default">';
				echo '<span class="sortable-count">' . $marker . '</span>';
					echo get_the_title();
					echo '<a href="' . get_permalink() . '?cat=' . $current_cat . '" title="View Product">View Product</a>';
				echo '</li>';
			endwhile;
		        echo '</ul>';
		    echo '</div>';
		    echo '
	    		<div class="acf-save-order">
	    			<span>Click the button to save any ordering changes prior to updating this page</span>
					<input class="button button-primary acf-save-order-button" type="button" value="Set Order">
				</div>
				<div class="check-success">
					<p></p>
				</div>
				<hr>
		    	';
		else :
			echo '<p>There are no Products associated with this Category.<br>';
			echo 'Try <a href="' . site_url("/wp-admin/post-new.php?post_type=products") . '">Adding a Product</a></p>';
		endif;
		/* Restore original Post Data */
		wp_reset_postdata();

	} 

	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add css + javascript to assist your create_field() action.
	*
	*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function input_admin_enqueue_scripts()
	{
		wp_register_style( 
			'post-list-acf', 
	    	get_template_directory_uri() . '/fields/fields.css', 
	    	array(), 
	    	'1', 
	    	'all' 
	    );

		  // enqueing:
		  wp_enqueue_style( 'post-list-acf' );
	}



}


// create field
new acf_field_post_list();

?>