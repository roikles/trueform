<?php 

/**
 * @author Ash Davies <ash.davies@outlook.com>
 */

require_once('../../../../wp-config.php');  
$wp->init();  
$wp->parse_request();  
$wp->query_posts();  
$wp->register_globals();


if (!empty($_POST)) {
	$cat = array_keys($_POST);
	$cat_ID = $cat[0];
	$posts = $_POST[$cat_ID];

	$s_posts = maybe_serialize($posts);

	add_custom_post_order($cat_ID,$s_posts);

}else{
	$title = "Access Denied";
	$message = "You do not have access to this page";
	$args = array(
		'response' => 403,
		'back_link' => true
		);
	wp_die( $message, $title, $args);
}