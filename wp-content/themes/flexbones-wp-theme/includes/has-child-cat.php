<?php $i = 1; ?>
<?php foreach($categories as $category) : ?> 

<?php
	//Fetch single post for every category 
	$posts = custom_cat_order($category->cat_ID);

	if ($posts[0]) {
		$post_id = $posts[0];
		$child_post = get_post($post_id);
	}else{
		$child_post = get_posts( array(
			'numberposts'		=>	1,
			'category'			=>	$category->cat_ID,
			'post_type'			=>	'products',
			'post_status'		=>	'publish' 
		)); 

		$post = $child_post[0];
		$post_id = $post->ID;
	}

?>

	<?php //Display thumb with link to child category?>
	<div class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 0){ echo 'product-carousel__thumb--m-last'; } ?> <?php if($i % 5 == 0){ echo 'product-carousel__thumb--d-last'; }; ?>">

			<a href="<?php echo get_post_permalink($post_id); ?>?cat=<?php echo $category->cat_ID; ?>">
			
			<?php //Fetch Taxonomy img using plugin
				$cat_id = $category->cat_ID;
				$imgs = get_option( 'taxonomy_image_plugin' );
				$thumb_info = wp_get_attachment_image_src( $imgs[$cat_id], 'thumbnail' );
				$thumb = $thumb_info[0];
			?>

			<?php if (is_url_exist($thumb)) : ?>
				<img src="<?php echo $thumb; ?>" class="product-carousel-thumb__image" alt=""/>
			<?php else : ?>
				<img src="http://placehold.it/243x148&amp;text=No%20Image%20Available" class="product-carousel-thumb__image" alt="">
			<?php endif; ?>

				<div class="product-carousel__thumb-caption"><?php echo $category->name; ?></div>

			</a>

		<?php //endforeach ?>

	</div>

	<?php if ($i % 15 == 0): ?>
		</li>
		<?php if($i !== $child_post->post_count): ?>
			<li class="slide">
		<?php endif; ?>
	<?php endif; ?>
	<?php $i++; ?>

<?php endforeach; ?>

<?php if (is_category('21')): ?>
	<? $cat_id = '24'; ?>

	<?//Fetch single post for every category 
	$posts = custom_cat_order($cat_id);

	if ($posts[0]) {
		$post_id = $posts[0];
		$child_post = get_post($post_id);
	}else{
		$child_post = get_posts( array(
			'numberposts'		=>	1,
			'category'			=>	$cat_id,
			'post_type'			=>	'products',
			'post_status'		=>	'publish' 
		)); 

		$post = $child_post[0];
		$post_id = $post->ID;
	}


	//Display thumb with link to child category ?>
	<div class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 0){ echo 'product-carousel__thumb--m-last'; } ?> <?php if($i % 5 == 0){ echo 'product-carousel__thumb--d-last'; }; ?>">

			<a href="<?php echo get_post_permalink($post_id); ?>?cat=<?php echo $cat_id; ?>">
			
			<?php //Fetch Taxonomy img using plugin
				$imgs = get_option( 'taxonomy_image_plugin', $cat_id);
				$thumb_info = wp_get_attachment_image_src( $imgs[$cat_id], 'thumbnail' );
				$thumb = $thumb_info[0];
			?>

			<?php if (is_url_exist($thumb)) : ?>
				<img src="<?php echo $thumb; ?>" class="product-carousel-thumb__image" alt=""/>
			<?php else : ?>
				<img src="http://placehold.it/243x148&amp;text=No%20Image%20Available" class="product-carousel-thumb__image" alt="">
			<?php endif; ?>

				<div class="product-carousel__thumb-caption"><?php echo get_cat_name( $cat_id ) ?></div>

			</a>

		<?php //endforeach ?>

	</div>
<?php endif ?>