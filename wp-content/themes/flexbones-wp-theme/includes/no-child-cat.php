<?php $i = 1; ?>
<?php
	$wpq = custom_query_order($category->cat_ID);
?>

<?php if ( $wpq->have_posts() ) : while ( $wpq->have_posts() ) : $wpq->the_post(); ?>
	<?php if($post->post_content !== ""): ?>

		<?php //Display Thumb with product rather than category link ?>
		<div class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 0){ echo 'product-carousel__thumb--m-last'; } ?> <?php if($i % 5 == 0){ echo 'product-carousel__thumb--d-last'; }; ?>">
			<a href="<?php the_permalink(); ?>?cat=<?php echo $top_cat_id; ?>">

			<?php if(get_field('product_slider')) : while(the_repeater_field('product_slider')) : ?>

				<?php
				// Pull in first image from ACF
					$rows = get_field('product_slider');
					$first_row = $rows[0];
					$first_row_image = $first_row['images'];
				?>
	
		    	<?php 
		    	// Declare Size and generate URL
		    		$size = "thumbnail";
		    		$image = wp_get_attachment_image_src( $first_row_image, $size );
		    	?>

				    
				<?php endwhile; ?>

					<?php if (is_url_exist($image[0])) : ?>
						<img src="<?php echo $image[0]; ?>" alt=""/>
					<?php else : ?>
						<img src="http://placehold.it/243x148&amp;text=No%20Image%20Available" alt="">
					<?php endif; ?>

				<?php endif; ?>

				<div class="product-carousel__thumb-caption"><?php the_title(); ?></div>
			</a>
		</div>

		
		<?php if ($i % 15 == 0): ?>
				</li>

			<?php if($i !== $post->post_count): ?>
				<li class="slide">
			<?php endif; ?>

		<?php endif; ?>
		<?php $i++; ?>
		
	<?php endif; ?>
<?php endwhile; endif; ?>
