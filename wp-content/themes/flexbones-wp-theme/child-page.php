<?php
	/*
		Template Name: Child Page Template
	*/
?>
<?php get_header(); ?>

	<section class="page-content content--products" role="main"> 
		<div class="inner">
			
			<?php // SLIDER CONTAINER ?>
			
			<div class="product-slider-container">
				
				<?php // red catalogue tab ?>
				<div class="catalogue-tab">
				<span class="helper"></span>
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>
				</div>
				<div class="slider big-image">
					<ul>
					<?php $img = get_the_post_thumbnail($post->ID,'main-product'); ?>
					<li><?php echo $img; ?></li>
					</ul>
				</div>
			</div>
			<?php // MAIN TEXT ?>

			<div class="product-text">
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

					<div class="content">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>

				<?php endwhile; ?>
				<?php endif; ?>

			</div>
		</div>

		<?php // CAROUSEL ?>

		<div class="products-wrap">
			<?php $id = wp_get_post_parent_id( $post->ID ); ?>
			<h2 class="products__heading"><?php echo get_the_title( $id ); ?></h2>
		</div>	

		<div class="products-carousel-container loading">
			
			<ul class="slides">
				<li class="slide">
					<?php $i = 1; ?>
				
					<?php
						$args = array(
								'child_of'		=> $id,
								'sort_column'	=> 'menu_order',
								'sort_order'	=> 'ASC'
							);
						$company = get_pages( $args );
					?>
	
					<?php foreach ($company as $child): ?>
	
					<?php $childtitle = get_the_title($child); ?>
	
						<div class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 0){ echo 'product-carousel__thumb--m-last'; }; if($i % 5 == 0){ echo 'product-carousel__thumb--d-last'; }; ?>">
	
							<a href="<?php echo post_permalink($child) ?>">
								<?php $img = get_the_post_thumbnail($child->ID,'thumbnail'); ?>
								<?php if ($img) : ?>
									<?php echo $img; ?>	
								<?php else : ?>
									<img src="http://placehold.it/243x148&amp;=No%20Image" alt="<?php echo $childtitle; ?>">
								<?php endif ; ?>
							</a>
							
							<div class="product-carousel__thumb-caption"><?php echo $childtitle; ?></div>
						</div>
						<?php $i++; ?>
	
					<?php endforeach ?>
	
					<?php if ($i % 15 == 0): ?>
					</li>

						<?php if($i !== $company->post_count): ?>
							<li class="slide">
						<?php endif; ?>

					<?php endif; ?>

				<?php if($i==$company->post_count): ?>
					</li>
				<?php endif; ?>

			</ul>
		</div>

	
	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>