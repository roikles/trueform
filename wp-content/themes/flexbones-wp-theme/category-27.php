<?php get_header(); ?>
<?php /* Template for Electronic Displays RTI & Kiosks Category */ ?>

	<section class="main main-index" role="main"> 

		<div class="inner">

			<div class="home-hero">
					<div class="catalogue-tab">
					<span class="helper"></span>
						<a href="http://www.ferrograph.com" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ferrograph.jpg" alt="Click here to see visit Ferrograph"></a>
					</div>
				<div class="slider big-image">
					<ul>
						<li>
						<?php //Fetch img for current category and display full width?>
						<?php 
							$category = get_category( get_query_var( 'cat' ) );
							$cat_id = $category->cat_ID;
							$imgs = get_option( 'taxonomy_image_plugin' );
						?>

						<?php if (wp_get_attachment_image( $imgs[$cat_id] )): 

							$cover_img =  wp_get_attachment_image_src( $imgs[$cat_id], 'category-parent-cover' ); ?>

							<img src="<?php echo $cover_img[0]; ?>">

						<?php else : ?>

							<img  src="http://placehold.it/1016x380&amp;=Placeholder" alt="">

						<?php endif; ?>
						
						</li>
					</ul>
				</div>

				<div class="main__brief">
					<h1><?php echo single_cat_title(); ?></h1>

					<?php //Use Category Description for the block of text overlayed on image ?>
					<?php echo category_description( $cat_id ); ?>
					</div>
	
				</div>


				<?php 
					// Top level category
					$cat = get_category( get_query_var( 'cat' ) );
					$top_cat_id = $cat->cat_ID;
					$args = array( 'child_of' => $top_cat_id);
				?>
				<?php 
					// Get child categories
					get_categories($args); 
					$categories = get_categories($args); 
				?>
			</div>

			<div class="products-wrap">
				<h2 class="products__heading">
					Products / <?php echo $category->cat_name; ?>
				</h2>
				<h2 class="slide-count">
					<span class="current-slide">
						1
					</span>
					 of 
					<span class="total-slides">
						1
					</span>
				</h2>
			</div>

			<div class="products-carousel-container loading">
				
				<ul class="slides">
				<li class="slide">

		<?php // If there are sub categories
			if(!empty($categories)) : 
		?>

			<?php require 'includes/has-child-cat.php'; ?>

		<?php else : // If there are NOT sub categories ?>

			<?php require 'includes/no-child-cat.php'; ?>

		<?php endif; ?>

			<?php if($i==$post->post_count): ?>
				</li>
			<?php endif; ?>
				</ul>
				
			</div>	
				
	</section>
		<?php //get_sidebar(); ?>	

<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>
