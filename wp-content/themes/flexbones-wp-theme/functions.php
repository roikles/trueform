<?php

function my_register_fields()
{
	include_once('fields/posts-v4.php');
}
add_action('acf/register_fields', 'my_register_fields');

/*==================================================================== */
/* CONTENT WIDTH
/*==================================================================== */

	if ( ! isset( $content_width )){
		$content_width = 620; //GLOBAL CONTENT WIDTH (px)
	}


/*==================================================================== */
/* CBG THEME SETUP
/*==================================================================== */


	add_action( 'after_setup_theme', 'flexbones_setup' );

	function flexbones_setup() {
	
		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style('editor-style.css?' . time());
	
		// Post Format support. You can also use the legacy "gallery" or "asides" (note the plural) categories.
		add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
	
		// This theme uses post thumbnails
		add_theme_support( 'post-thumbnails' );
	
		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );
	
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => 'Primary Navigation',
		) );
		
		//register sidebars
		register_sidebar();
	}

/*==================================================================== */
/* SET THE EXCERPT LENGTH
/*==================================================================== */

	function cbg_excerpt_length( $length ) {
		if (is_front_page()) :
			return 0;
		else :
			return 55;
		endif;
	}
	
	add_filter( 'excerpt_length', 'cbg_excerpt_length' );
	

/*==================================================================== */
/* SET THE READ MORE TEXT
/*==================================================================== */

	/* Replaces the excerpt "more" text by a link */

	function new_excerpt_more($more) {
	    global $post;
	    return '<a href="'. get_permalink($post->ID) . '" class="read-more">' . 'Read more...' . '</a>';
	}
	add_filter('excerpt_more', 'new_excerpt_more');


/*==================================================================== */
/* Remove inline styles printed when the gallery shortcode is used
/*==================================================================== */

	add_filter( 'use_default_gallery_style', '__return_false' );
	
/*==================================================================== */
/* ADD GALLERY THUMBNAIL CUSTOM SIZE 
/*==================================================================== */

add_image_size( 'main-product', '500', '500', true );
add_image_size( 'homepage-slider', '1016', '380', true );
add_image_size( 'category-parent-cover', '1016', '380', true );

/*==================================================================== */
/* STOP TINY MCE Editing BRS
/*==================================================================== */

function cbnet_tinymce_config( $init ) {

    // Don't remove line breaks
    $init['remove_linebreaks'] = false; 

    // Pass $init back to WordPress
    return $init;
}
add_filter('tiny_mce_before_init', 'cbnet_tinymce_config');

/*==================================================================== */
/* REMOVE Paragraphs round IMAGES 
/*==================================================================== */

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');


/*==================================================================== */
/* REMOVE ADMIN BAR 
/*==================================================================== */

add_filter('show_admin_bar', '__return_false');

/*==================================================================== */
/* SUB MENU
/*==================================================================== */	
	
function sub_menu(){
	
	global $post;
	
	if($post){
		
		if($post->post_parent){
			$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
		} else {
			$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
		} 
		
		if ($children) {
			echo '<ul>' . $children . '</ul>';
		}
	}
}

/*==================================================================== */
/* Add parent class to Wp list pages! great for recursive menus!
/*==================================================================== */	

function add_parent_class( $css_class, $page, $depth, $args ){
    if ( ! empty( $args['has_children'] ) )
        $css_class[] = 'parent slide_down';
    return $css_class;
}
add_filter( 'page_css_class', 'add_parent_class', 10, 4 );

/*==================================================================== */
/* Add parent class to wp_nav_menu
/*==================================================================== */       

add_filter('wp_nav_menu_objects', function ($items) {

    $hasSub = function ($menu_item_id, $items) {
        foreach ($items as $item) {
            if ($item->menu_item_parent && $item->menu_item_parent == $menu_item_id) {
                return true;
            }
        }
        return false;
    };

    foreach ($items as $item) {
        if ($hasSub($item->ID, $items)) {
            $item->classes[] = 'parent';
        }
    }
    return $items;    

});

/*==================================================================== */
/* Wrap Li's in spans from the content to allow for greater styling control
/*==================================================================== */	

function span_li($content){
	
	$content = str_replace( '<li>','<li><span>',$content );
	$content = str_replace( '</li>','</span></li>',$content );
	return $content;
	
}

add_filter( 'the_content', 'span_li' );

/*==================================================================== */
/* Enque JS Files 
/*==================================================================== */

function barebones_load_js() {
 	//register sitewide scripts and request JQUERY 1.8.3 as a dependency
 	// NAME / LOCATION / DEPENDENCIES (accepts array) / VERSION / IN FOOTER (true | false)
 	wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', array(), '1.10.2', true );
 	wp_register_script( 'jquery-ui','//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array(), '1.10.3', true);
	wp_register_script( 'sitewide-scripts', get_template_directory_uri( ) . '/js/scripts-min.js', array( 'jquery' ), '1', true );
	wp_register_script('modernizr', '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js', array(), '2.7.1', false);
	wp_register_script('selectivizr', '//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js', array(), '1.0.2', false);
	wp_register_script('flexslider','//cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.0/jquery.flexslider-min.js', array('jquery'), '2.2.0', true);

  	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-ui' );
	wp_enqueue_script('modernizr');
	wp_enqueue_script( 'sitewide-scripts' );
	wp_enqueue_script( 'selectivizr');
	wp_enqueue_script('flexslider');

}
 
add_action('wp_enqueue_scripts', 'barebones_load_js'); // For use on the Front end (ie. Theme)

function remove_wp_jquery() {
	//if statements prevents it deregistering in admin
	if( !is_admin() ) {
		wp_deregister_script('jquery');
		wp_deregister_script('jquery-ui');
	}
}

add_action('init', 'remove_wp_jquery'); // will deregister from head




/**
 * Localise Vars
 * Make specified PHP data available in your specified scripts
 */

function localize_vars() {
	$stylesheet_root = array( 'dir' => get_stylesheet_directory_uri() );
	wp_localize_script( 'sitewide-scripts', 'stylesheet_root', $stylesheet_root );
	wp_localize_script( 'gridtacular', 'stylesheet_root', $stylesheet_root );
}


/*==================================================================== */
/* Enque Stylesheet
/*==================================================================== */

function stylesheet_loader() {
	wp_register_style( 
		'page-style', 
    	get_template_directory_uri() . '/style.css', 
    	array(), 
    	'2', 
    	'all' 
    );

  // enqueing:
  wp_enqueue_style( 'page-style' );


}

add_action( 'wp_enqueue_scripts', 'stylesheet_loader' );

/**
 * Remove standard image sizes so that these sizes are not
 * created during the Media Upload process
 *
 * Tested with WP 3.2.1
 *
 * Hooked to intermediate_image_sizes_advanced filter
 * See wp_generate_attachment_metadata( $attachment_id, $file ) in wp-admin/includes/image.php
 *
 * @param $sizes, array of default and added image sizes
 * @return $sizes, modified array of image sizes
 * @author Ade Walker http://www.studiograsshopper.ch
 */
function sgr_filter_image_sizes( $sizes) {

	unset( $sizes['medium']);
	unset( $sizes['large']);
	unset( $sizes['detail']);
	
	return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'sgr_filter_image_sizes');

/**
 * Force GravityForms to load jQuery in footer
 */

function init_scripts() {
	return true;
}

add_filter("gform_init_scripts_footer", "init_scripts");


/**
 * Create a custom Post Type
 * Much better than using a plugin
 * @author Rory Ashford <rory@roikles.com>
 */
add_action('init', 'register_mypost_type');
function register_mypost_type() {
register_post_type(
	'products', array(	
		'label' 			 => 'Products',
		'description' 		 => 'Add products to the site.',
		'public' 			 => true,
		'show_ui' 			 => true,
		'show_in_menu' 		 => true,
		'capability_type' 	 => 'post',
		'show_in_nav_menus'  => true,
		'hierarchical' 		 => false,
		'rewrite' 			 => array(
			'slug' 				=> '',
			'with_front' 		=> '0'
		),
		'query_var' 		 => true,
		'has_archive' 		 => true,
		'supports' 			 => array(
			'title',
			'editor',
			'revisions',
			'thumbnail',
			'author',
			'page-attributes',
		),
		'taxonomies' 		=> array(
			'category'
		),
		'labels' 			=> array(
			'name' 				 => 'Products',
			'singular_name' 	 => 'Product',
			'menu_name' 		 => 'Products',
			'add_new' 			 => 'Add Product',
			'add_new_item' 		 => 'Add New Product',
			'edit' 				 => 'Edit',
			'edit_item' 		 => 'Edit Product',
			'new_item' 			 => 'New Product',
			'view' 				 => 'View Product',
			'view_item' 		 => 'View Product',
			'search_items' 		 => 'Search Products',
			'not_found' 		 => 'No Products Found',
			'not_found_in_trash' => 'No Products Found in Trash',
			'parent' 			 => 'Parent Product'
		),
	) 
);
}

/**
 * Create a custom Taxonomy
 * Much better than using a plugin
 * @author Rory Ashford <rory@roikles.com>
 */

register_taxonomy(
	'regions',array (
  		0 				=> 'regions',
	),array( 
		'hierarchical'   => false,
		'label' 		 => 'Regions',
		'show_ui' 		 => true,
		'query_var' 	 => true,
		'rewrite' 		 => array(
			'slug' 			=> '',
			'hierarchical' 	=> true
		),
		'singular_label' => 'Region'
	) 
);

/**
 * Create a custom Post Type
 * Much better than using a plugin
 * @author Rory Ashford <rory@roikles.com>
 */
add_action('init', 'register_mysecondpost_type');
function register_mysecondpost_type() {
register_post_type(
	'vacancies', array(	
		'label' 			 => 'Vacancies',
		'description' 		 => 'Add Vacancies to the site.',
		'public' 			 => true,
		'show_ui' 			 => true,
		'show_in_menu' 		 => true,
		'capability_type' 	 => 'post',
		'show_in_nav_menus'  => true,
		'hierarchical' 		 => false,
		'rewrite' 			 => array(
			'slug' 				=> '',
			'with_front' 		=> '0'
		),
		'query_var' 		 => true,
		'has_archive' 		 => true,
		'supports' 			 => array(
			'title',
			'editor',
			'revisions',
			'thumbnail',
			'author',
			'page-attributes',
		),
		'taxonomies' 		=> array(
			'regions'
		),
		'labels' 			=> array(
			'name' 				 => 'Vacancies',
			'singular_name' 	 => 'Vacancy',
			'menu_name' 		 => 'Vacancies',
			'add_new' 			 => 'Add Vacancy',
			'add_new_item' 		 => 'Add New Vacancy',
			'edit' 				 => 'Edit',
			'edit_item' 		 => 'Edit Vacancy',
			'new_item' 			 => 'New Vacancy',
			'view' 				 => 'View Vacancy',
			'view_item' 		 => 'View Vacancy',
			'search_items' 		 => 'Search Vacancies',
			'not_found' 		 => 'No Vacancies Found',
			'not_found_in_trash' => 'No Vacancies Found in Trash',
			'parent' 			 => 'Parent Vacancy'
		),
	) 
);
}

function custom_query_order($catID,$posts_per_page = '-1'){
	global $wpdb;
	// Check to see if values exist
	$sql_check = "SELECT post_order
				FROM wp_category_order
				WHERE category_id = '" . $catID . "'
				";

	$order = $wpdb->get_results($sql_check,ARRAY_A);
	$check_order = $order[0]['post_order'];

	// If order does not exist
		if (empty($check_order)) {
			// Set default Args
			$args = array(
				'post_type' => 'products',
				'post_status' => 'publish',
				'posts_per_page' => $posts_per_page,
				'cat' => $catID
			);
			// The Query
			$the_query = new WP_Query( $args );

			return $the_query;

		} else { // Otherwise

			// Set custom order
			global $posts; 
			$specific_posts = maybe_unserialize( $check_order );
			$posts = $specific_posts;
			$args = array(
			    'post__in'	=> $specific_posts,
			    'posts_per_page'   => $posts_per_page,
			    'post_type'	=>	'products',
			    'post_status'	=>	'publish',
			    'cat'	=>	$catID
			);

			// The Query
			$the_query = new WP_Query( $args );

			//Order an array of objects by object property
			function orderby( $a, $b ) {
			    global $posts;
			    $apos   = array_search( $a->ID, $posts );
			    $bpos   = array_search( $b->ID, $posts );
			    return ( $apos < $bpos ) ? -1 : 1;
			}
			usort( $the_query->posts, "orderby" );

			return $the_query;

		}
}

function custom_cat_order($catID){
	global $wpdb;
	// Check to see if values exist
	$sql_check = "SELECT post_order
				FROM wp_category_order
				WHERE category_id = '" . $catID . "'
				";

	$order = $wpdb->get_results($sql_check,ARRAY_A);
	$check_order = $order[0]['post_order'];

	// If order does not exist
	if (!empty($check_order)) {
		// Set custom order
		global $posts; 
		$specific_posts = maybe_unserialize( $check_order );
		$posts = $specific_posts;
		return $posts;
	}
}



function is_url_exist($url){
    $ch = curl_init($url);    
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if($code == 200){
       $status = true;
    }else{
      $status = false;
    }
    curl_close($ch);
   return $status;
}

function check_for_additions($current_cat){

	$existing_posts = array();

	// Set default Args
	$args = array(
		'post_type' => 'products',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'cat' => $current_cat
	);
	// The Query
	$posts = new WP_Query( $args );

	$i = 0; while ($posts->have_posts()) {
		$posts->the_post();
		$existing_posts[$i] = get_the_ID();
		$i++;
	}

	wp_reset_postdata();

	global $wpdb;

		$check_sql = "SELECT post_order
			FROM wp_category_order
			WHERE category_id = '" . $current_cat . "'";

		$check = $wpdb->get_results($check_sql);

		// Overwrite if they do
		if ($check) {
			$check_key = $check[0];
			$check_order = $check_key->post_order;
			$ordered_posts = maybe_unserialize($check_order);
		};

	$diff = array_diff($existing_posts, $ordered_posts);

	if (empty($diff)) {
		return false;
	}else{
		$count = count($ordered_posts);
	
		foreach ($diff as $key => $value) {
			$ordered_posts[$count] = $value;
			$count++;
		}

		$posts = maybe_serialize($ordered_posts);

		add_custom_post_order($current_cat,$posts);
	}

}


function check_for_removals($current_cat){

	$existing_posts = array();

	// Set default Args
	$args = array(
		'post_type' => 'products',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'cat' => $current_cat
	);
	// The Query
	$posts = new WP_Query( $args );

	$i = 0; while ($posts->have_posts()) {
		$posts->the_post();
		$existing_posts[$i] = get_the_ID();
		$i++;
	}

	wp_reset_postdata();

	global $wpdb;

	$check_sql = "SELECT post_order
		FROM wp_category_order
		WHERE category_id = '" . $current_cat . "'";

	$check = $wpdb->get_results($check_sql);

	// Overwrite if they do
	if ($check) {
		$check_key = $check[0];
		$check_order = $check_key->post_order;
		$ordered_posts = maybe_unserialize($check_order);
	};

	$diff = array_diff($ordered_posts, $existing_posts);

	if (empty($diff)) {
		return false;
	}else{
		$count = count($ordered_posts);

		foreach ($diff as $key => $value) {
			if (array_key_exists($key, $ordered_posts)) {
				unset($ordered_posts[$key]);
			}
		}

		$ordered_posts = array_values($ordered_posts);

		$posts = maybe_serialize($ordered_posts);
		
		add_custom_post_order($current_cat,$posts);
	}
}

function add_custom_post_order($cat_ID,$posts){

	global $wpdb;

	$table = 'wp_category_order';

	if(mysql_num_rows(mysql_query("SHOW TABLES LIKE '" . $table . "'"))!=1) :
		// echo "Table does not exist";
		$sql = "CREATE TABLE " . $table . " (
		  category_id INT(20) NOT NULL PRIMARY KEY,
		  post_order VARCHAR(255) NOT NULL
		);";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	endif;	

	// Check to see if values exist
	$sql_check = "SELECT post_order
				FROM wp_category_order
				WHERE category_id = '" . $cat_ID . "'";

	// Overwrite if they do
	if ($check_meta = $wpdb->get_results($sql_check)) {
		$wpdb->update(
		// Table Name
		$table,	
			// Column Headings
			array(
				'post_order'	=>	$posts,
			),
			// Which row? (WHERE SQL)
			array(
				'category_id'	=>	$cat_ID,
			)
		);
	// Else insert
	}else{
		$wpdb->insert(
		$table,	
			array(
				'category_id'	=>	$cat_ID,
				'post_order'	=>	$posts
			)
		);
	}

	return true;
}


/**
 * Remove bloat from wordpress html head
 */

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
//remove_action('wp_head', 'feed_links');
//remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

