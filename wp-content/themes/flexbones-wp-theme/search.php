<?php /* The template for displaying Search Results pages. */ ?>
<?php get_header(); ?>
<?php $searchterm = $_GET['s']; ?>
<?php global $wp_query; ?>
	<section class="page-content" role="main">
		<div class="search-inner inner">

		<section class="section-title">
			<h1>Search Results for "<?php echo $searchterm; ?>" ( <?php echo $wp_query->found_posts; ?> found )</h1>
		</section>
	
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
			    <article class="post">
			    	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			        <?php the_excerpt(); ?>
			    </article>
			    
			<?php endwhile; ?>
	
				<div class="pagination">
					<?php echo paginate_links( $args ); ?> 
				</div>
	
			<?php endif; ?>
	
		</div>
	</section>
<?php get_footer(); ?>
