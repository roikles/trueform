<?php get_header(); ?>
<?php /* Template for Vacancies */ ?>

	<section class="main main-index" role="main"> 
		<div class="inner">
			<div class="home-hero">
				<div class="catalogue-tab">
				<span class="helper"></span>
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/documents/Trueform%20Products%20&amp;%20Services%20Catalogue%202013%20Print%20Version.pdf.zip" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/catalogue.png" alt="Click here to see our catalogue"></a>

				</div>
				<ul>
					<li class="category-parent-cover">
						<?php if (has_post_thumbnail()): ?>
							<?php the_post_thumbnail( 'parent-category-cover' ); ?>
						<?php else : ?>
							<img src="http://placehold.it/1016x380&amp;=%20" alt="">
						<?php endif; ?>							
					</li>
				</ul>
				<div class="main__brief">
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
				</div>
	
			</div>
			<?php // MAIN TEXT ?>
		</div>

		<?php // CAROUSEL ?>

		<div class="products-carousel-container loading">
			<h2 class="products__heading"><?php echo get_the_title(); ?></h2>
			<ul>
			
				<?php
					$id = get_the_ID();
					$depots = get_pages( 'child_of='.$id );
				?>

				<?php foreach ($depots as $child): ?>

				<?php $childtitle = get_the_title($child); ?>

					<li class="product-carousel__thumb <?php if($i % 2 == 0){ echo 'product-carousel__thumb--xs-m-last'; }?> <?php if($i % 3 == 2){ echo 'product-carousel__thumb--mobile-up-end'; } ?> <?php if(($i + 1) % 5 == 0){ echo 'product-carousel__thumb--tablet-up-end'; } ?>">

						<a href="<?php echo post_permalink($child) ?>">
							<?php $img = get_the_post_thumbnail($child->ID,'thumbnail'); ?>
							<?php if ($img) : ?>
								<?php echo $img; ?>	
							<?php else : ?>
								<img src="http://placehold.it/200x148" alt="<?php echo $childtitle; ?>">
							<?php endif ; ?>
						</a>
						
						<div class="product-carousel__thumb-caption"><?php echo $childtitle; ?></div>
					</li>

				<?php endforeach ?>

			</ul>

		</div>

	
	</section>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>